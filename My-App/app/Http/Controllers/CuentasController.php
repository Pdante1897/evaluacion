<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuentas;
use Illuminate\Support\Facades\DB;

class CuentasController extends Controller
{
    public function mostrarCuenta(Request $request){

        $idcuenta = $request -> input('idcuenta');
        $cuentas = Cuentas::find($idcuenta);
        if ($cuentas == null){
            $mensaje = "La cuenta es inexistente";
            return view('Cuentas', compact('mensaje'));
        }
        $mensaje = "el saldo de la cuenta ". $cuentas -> id . " es: " . $cuentas-> saldo;
        $resultados = DB::select('SELECT * FROM transaccion WHERE cuentas_id = '. $idcuenta . ' ORDER BY fechahora DESC LIMIT 10;');

        return view('Cuentas')
            ->with('mensaje', $mensaje)
            ->with('resultados', $resultados);
    }
}
