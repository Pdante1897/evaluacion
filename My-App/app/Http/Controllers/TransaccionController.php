<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaccionController extends Controller
{
    public function transaccion(Request $request)
    {
        $idsucursal = $request -> input('idsucursal');
        $idcuenta = $request -> input('idcuenta');
        $descripcion = $request -> input('descripcion');
        $monto = $request -> input('monto');
        $mensaje = "Cliente o cuenta inexistente";
    
        try {
            $resultado = DB::select('CALL realizar_transaccion(?, ?, ?, ?)', [$descripcion, $monto, $idcuenta, $idsucursal]);
            $mensaje = "Transaccion exitosa";
    
        } catch (\Exception $e) {
            $mensaje = "Error al realizar la transaccion";
        }
        
        return view('transaccion', compact('mensaje'));
    }
}
