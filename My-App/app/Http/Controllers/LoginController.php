<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empleado;

class LoginController extends Controller
{
    public function login(Request $request){

        $idempleado = $request -> input('idempleado');
        $password = $request -> input('password');

        $empl = Empleado::find($idempleado);
        if ($empl == null){
            $mensaje = "Empleado es inexistente";
            return view('login', compact('mensaje'));
        }
        if($empl -> id == $idempleado && $empl -> pass == $password ){
            $mensaje = $empl -> nombre . ' ' . $empl -> apellidos;

            return view('menu', compact('mensaje'));
        }
        $mensaje = "Credenciales no coinciden";

        return view('login', compact('mensaje'));
    }
}
