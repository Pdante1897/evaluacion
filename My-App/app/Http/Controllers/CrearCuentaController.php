<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrearCuentaController extends Controller
{
    public function crearCuenta(Request $request)
{
    $idcliente = $request -> input('idcliente');
    $moneda = $request -> input('moneda');
    $mensaje = "Cliente inexistente";

    try {
        $resultado = DB::select('CALL crear_cuenta(?, ?)', [$idcliente, $moneda]);
        $mensaje = "Creacion exitosa";

    } catch (\Exception $e) {
        
    }
    
    return view('crearCuentas', compact('mensaje'));
}
}
