<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CuentasController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CrearCuentaController;
use App\Http\Controllers\TransaccionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/menu', function () {
    return view('menu');
})->name('menu');

Route::post('/mostrarCuenta', [CuentasController::class, 'mostrarCuenta'])->name('mostrarCuenta');


Route::get('/mostrarCuenta', function () {
    $resultados = " ";

    return view('Cuentas');
});

Route::post('/crearCuenta', [CrearCuentaController::class, 'crearCuenta'])->name('crearCuenta');


Route::get('/crearCuenta', function () {
    $mensaje = " ";
    return view('crearCuentas');
});


Route::post('/transaccion', [TransaccionController::class, 'transaccion'])->name('transaccion');


Route::get('/transaccion', function () {
    $mensaje = " ";
    return view('transaccion');
});

Route::get('/login', function () {
    return view('login');
});

Route::post('/login', [LoginController::class, 'login'])->name('login');
