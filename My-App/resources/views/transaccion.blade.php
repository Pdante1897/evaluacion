<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
  $('#idcuenta').on('input', function() {
    var valor = $(this).val();
    if (!/^\d*$/.test(valor)) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
  });

  $('#idsucursal').on('input', function() {
    var valor = $(this).val();
    if (!/^\d*$/.test(valor)) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
  });

  $('#monto').on('input', function() {
    var valor = $(this).val();
    if (!/^\d+(\.\d+)?$/.test(valor)) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
  });

 
  $('#transaccione').on('submit', function(event) {
    event.preventDefault();
    var valido = true;

    $(this).find('input').each(function() {
      if ($(this).hasClass('error')) {
        valido = false;
        $(this).focus();
        return false; // Detener el bucle
      }
    });

    if (valido) {
        this.submit();
    } else {
      alert('Hay errores en el formulario.');
    }
  });
});
</script>
<div style="text-align: center; margin-top: 100px;">
    <h1>Transacciones</h1>

    <form id= "transaccione" name = "transaccione"action="{{ route('transaccion') }}" method="POST">
        @csrf
            <div>
                <label for="idcuenta">Numero de Cuenta:</label>
                <input type="text" id="idcuenta" name="idcuenta" required>
            </div>
            <div>
                <label for="idsucursal">Numero Sucursal:</label>
                <input type="text" id="idsucursal" name="idsucursal" required>
            </div>
            <div>
                <label for="monto">Monto:</label>
                <input type="text" id="monto" name="monto" required>
            </div>
        <label for="descripcion">Seleccione tipo de transaccion:</label>
        <select name="descripcion" id="descripcion">
            <option value="deposito">Deposito</option>
            <option value="retiro">Retiro</option>
        </select>
        <button type="submit">Enviar</button>
    </form>
    @if(isset($mensaje))
        <h2>{{$mensaje}}</h2>
    @endif
    <a href="{{ route('menu') }}">Menu</a>
</div>
</body>
</html>