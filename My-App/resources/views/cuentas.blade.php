<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
  $('#idcuenta').on('input', function() {
    var valor = $(this).val();
    if (!/^\d*$/.test(valor)) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
  });

 
  $('#cuentas').on('submit', function(event) {
    event.preventDefault();
    var valido = true;

    $(this).find('input').each(function() {
      if ($(this).hasClass('error')) {
        valido = false;
        $(this).focus();
        return false; 
      }
    });

    if (valido) {
        this.submit();
    } else {
      alert('Hay errores en el formulario.');
    }
  });
});
</script>



<div style="text-align: center; margin-top: 100px;">
    <h1>Saldo de cuenta</h1>
            <form id = "cuentas" name = "cuentas" method="POST" action="{{ route('mostrarCuenta') }}">
                @csrf
                <div>
                    <label for="idcuenta">Cuenta:</label>
                    <input type="text" id="idcuenta" name="idcuenta" required>
                </div>
                <div>
                    <button type="submit">Consultar</button>
                </div>
            </form>
    @if(isset($mensaje))
        <h2>{{$mensaje}}</h2>
    @endif
    @if(isset($resultados))
        <h2>Ultimos 10 movimientos</h2>
        <ul>
            @foreach($resultados as $value)
                <li>Descripcion: {{$value -> descripcion}} - Monto: {{$value -> monto}} - Fecha y Hora: {{$value -> fechahora}} - Sucursal: {{$value -> sucursal_id}}  </li>
            @endforeach
        </ul>
    @endif

    <a href="{{ route('menu') }}">Menu</a>

</div>
</body>
</html>