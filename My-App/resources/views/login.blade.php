<!DOCTYPE html>
<html>
<head>
    <title>Iniciar sesión</title>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    
  $('#idempleado').on('input', function() {
    var valor = $(this).val();
    if (!/^\d*$/.test(valor)) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
  });

 
  $('#login').on('submit', function(event) {
    event.preventDefault();
    var valido = true;

    $(this).find('input').each(function() {
      if ($(this).hasClass('error')) {
        valido = false;
        $(this).focus();
        return false; 
      }
    });

    if (valido) {
        this.submit();
    } else {
      alert('Hay errores en el formulario.');
    }
  });
});
</script>
    <div style="text-align: center; margin-top: 100px;">
        <h1>Iniciar sesión</h1>
        <form id="login" name = "login" method="POST" action="{{ route('login') }}">
            @csrf
            <div>
                <label for="idempleado">Identificacion:</label>
                <input type="text" id="idempleado" name="idempleado" required>
            </div>
            <div>
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div>
                <button type="submit">Iniciar sesión</button>
            </div>
        </form>
        @if(isset($mensaje))
        <h2>{{$mensaje}}</h2>
        @endif
    </div>
</body>
</html>
