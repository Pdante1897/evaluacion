<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div style="text-align: center; margin-top: 100px;">
    <h1>Crear Cuenta</h1>

    <form action="{{ route('crearCuenta') }}" method="POST">
        @csrf
            <div>
                <label for="idcliente">Numero de identificacion:</label>
                <input type="text" id="idcliente" name="idcliente" required>
            </div>
        <label for="moneda">Seleccione la moneda:</label>
        <select name="moneda" id="moneda">
            <option value="Q">Q</option>
            <option value="$">$</option>
        </select>
        <button type="submit">Enviar</button>
    </form>
    @if(isset($mensaje))
        <h2>{{$mensaje}}</h2>
    @endif
    <a href="{{ route('menu') }}">Menu</a>
</div>
</body>
</html>