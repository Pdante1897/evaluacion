insert into tipo_documento values (1, 'DPI' ), (2, 'Pasaporte' );

insert into departamento values (1, 'Guatemala');

insert into municipio values (1, 'Guatemala', 1);
insert into municipio values (2, 'Mixco', 1);
insert into municipio values (3, 'Villa Nueva', 1);

insert into empleado values (12345678, 'Bryan Gerardo', 'Paez Morales', 5, 'evaluacion123');
insert into empleado values (12345679, 'Carla Martina', 'Lopez Duran', 6, 'evaluacion321');

insert into sucursal (nombre, zona, municipio_id, cajeros) values ('centro civico', 1, 1, 5);
insert into sucursal (nombre, zona, municipio_id, cajeros) values ('el zapote', 2, 1, 4);
insert into sucursal (nombre, zona, municipio_id, cajeros) values ('reforma', 10, 1, 7);
insert into sucursal (nombre, zona, municipio_id, cajeros) values ('aguilar batres', 11, 1, 8);

CALL insertar_cliente('5263987852369', 'Juan Alfonso', 'Perez Castos', NULL, 1, 1);
CALL insertar_cliente('2687635125698', 'Pedro Ricardo', 'Lopez Lopez', NULL, 1, 2);
CALL insertar_cliente('9003356A', NULL, NULL, 'Los pollos hermanos', 2, 3);


select * from empleado;
select * from cliente;
select * from cuentas;
select * from transaccion;
select * from sucursal;

CALL crear_cuenta('5263987852369', 'Q');
CALL crear_cuenta('2687635125698', 'Q');
CALL crear_cuenta('9003356A', '$');


---------- al borrar db el id no se reinicia----------
CALL realizar_transaccion('Deposito', 100.50, 4, 5);
CALL realizar_transaccion('Deposito', 100500, 5, 6);

CALL realizar_transaccion('Deposito', 16750.00, 6, 8);
CALL realizar_transaccion('Retiro', 16750.00, 10, 8);

--------------------------------consulta a-----------------------------------------
SELECT
    c.id AS cliente_id,
    c.nombres AS nombres_cliente,
    c.apellidos AS apellidos_cliente,
    COUNT(cuenta.id) AS total_cuentas,
    COUNT(CASE WHEN cuenta.activa = 'S' THEN 1 END) AS cuentas_activas,
    COUNT(CASE WHEN cuenta.activa = 'N' THEN 1 END) AS cuentas_no_activas
FROM
    cliente c
LEFT JOIN
    cuentas cuenta ON c.id = cuenta.cliente_id
GROUP BY
    c.id, c.nombres, c.apellidos;
	
	
--------------------------------consulta b-----------------------------------------
SELECT EXTRACT(MONTH FROM fechahora) AS mes,
       EXTRACT(YEAR FROM fechahora) AS anio,
       SUM(monto) AS saldo_total
FROM transaccion
GROUP BY EXTRACT(MONTH FROM fechahora), EXTRACT(YEAR FROM fechahora)
ORDER BY anio, mes;


------------------------------consulta c--------------------------------
SELECT c.id AS cliente_id, c.nombres, c.apellidos, c.razon, c.tipo_documento_id
FROM cliente c
JOIN cuentas cu ON c.id = cu.cliente_id
JOIN transaccion t ON cu.id = t.cuentas_id
WHERE cu.saldo > 20000
  AND Lower(t.descripcion) = 'deposito'
  AND t.fechahora >= NOW() - INTERVAL '15 days'
GROUP BY c.id, c.nombres, c.apellidos;

----------------------------------Consulta d---------------------------------
SELECT s.id AS agencia_id, s.nombre AS agencia_nombre, s.cajeros AS cantidad_cajeros
FROM cliente c
JOIN cuentas cu ON c.id = cu.cliente_id
JOIN transaccion t ON cu.id = t.cuentas_id
JOIN sucursal s ON t.sucursal_id = s.id
WHERE Lower(t.descripcion) = 'deposito'
  AND t.fechahora >= (NOW() - INTERVAL '5 days')
GROUP BY s.id, s.nombre, s.cajeros;

--------------------------------Consulta e----------------------------------------
SELECT c.id AS cliente_id, c.nombres, c.apellidos,
       EXTRACT(MONTH FROM t.fechahora) AS mes,
       SUM(t.monto) AS suma_montos,
       COUNT(*) AS cantidad_transacciones,
       COUNT(DISTINCT t.sucursal_id) AS cantidad_agencias
FROM cliente c
JOIN cuentas cu ON c.id = cu.cliente_id
JOIN transaccion t ON cu.id = t.cuentas_id
WHERE 
  (t.monto > 2000 AND cu.moneda = '$') OR
  (cu.moneda = 'Q' AND (t.monto > (2000 * 8) OR (Lower(t.descripcion) = 'deposito' AND t.monto > 10000))) OR
  (t.monto > 20000 AND cu.moneda = 'Q') OR
  (t.monto > (20000 * 8) AND cu.moneda = '$')
GROUP BY c.id, c.nombres, c.apellidos, mes
HAVING
  SUM(t.monto) > 2000 OR
  COUNT(*) > 10 OR
  COUNT(DISTINCT t.sucursal_id) > 10;
