DROP DATABASE evaluacion

CREATE DATABASE evaluacion;


CREATE TABLE cliente (
    id                VARCHAR(13) NOT NULL,
    nombres           VARCHAR(25) NULL,
    apellidos         VARCHAR(25) NULL,
    razon             VARCHAR(50),
    tipo_documento_id INTEGER NOT NULL,
    municipio_id      INTEGER NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY (id);

CREATE TABLE cuentas (
    id         SERIAL NOT NULL,
    cliente_id VARCHAR(13) NOT NULL,
    activa     CHAR(1) NOT NULL,
    moneda      CHAR(1) NOT NULL,
	saldo 		NUMERIC NOT NULL
);

ALTER TABLE cuentas ADD CONSTRAINT cuentas_pk PRIMARY KEY (id);

CREATE TABLE departamento (
    id     INTEGER NOT NULL,
    nombre VARCHAR(25) NOT NULL
);

ALTER TABLE departamento ADD CONSTRAINT departamento_pk PRIMARY KEY (id);

CREATE TABLE empleado (
    id          SERIAL NOT NULL,
    nombre      VARCHAR(30) NOT NULL,
    apellidos   VARCHAR(25) NOT NULL,
    sucursal_id INTEGER NOT NULL,
	pass varchar(25) NOT NULL
);

ALTER TABLE empleado ADD CONSTRAINT empleado_pk PRIMARY KEY (id);

CREATE TABLE municipio (
    id              INTEGER NOT NULL,
    nombre          VARCHAR(25) NOT NULL,
    departamento_id INTEGER NOT NULL
);

ALTER TABLE municipio ADD CONSTRAINT municipio_pk PRIMARY KEY (id);

CREATE TABLE sucursal (
    id               SERIAL NOT NULL,
    nombre           VARCHAR(20) NOT NULL,
    zona             INTEGER NOT NULL,
    municipio_id     INTEGER NOT NULL,
	cajeros INTEGER NOT NULL
);

ALTER TABLE sucursal ADD CONSTRAINT sucursal_pk PRIMARY KEY (id);

CREATE TABLE tipo_documento (
    id   INTEGER NOT NULL,
    tipo VARCHAR(15) NOT NULL
);


ALTER TABLE tipo_documento ADD CONSTRAINT tipo_documento_pk PRIMARY KEY (id);

CREATE TABLE transaccion (
    id          SERIAL NOT NULL,
    descripcion VARCHAR(50) NOT NULL,
    monto       NUMERIC NOT NULL,
    cuentas_id  INTEGER NOT NULL,
    fechahora   TIMESTAMP NOT NULL,
    sucursal_id INTEGER NOT NULL
);

ALTER TABLE transaccion ADD CONSTRAINT transaccion_pk PRIMARY KEY (id);

ALTER TABLE cliente
    ADD CONSTRAINT cliente_municipio_fk FOREIGN KEY (municipio_id)
        REFERENCES municipio (id);

ALTER TABLE cliente
    ADD CONSTRAINT cliente_tipo_documento_fk FOREIGN KEY (tipo_documento_id)
        REFERENCES tipo_documento (id);

ALTER TABLE cuentas
    ADD CONSTRAINT cuentas_cliente_fk FOREIGN KEY (cliente_id)
        REFERENCES cliente (id);

ALTER TABLE empleado
    ADD CONSTRAINT empleado_sucursal_fk FOREIGN KEY (sucursal_id)
        REFERENCES sucursal (id);

ALTER TABLE municipio
    ADD CONSTRAINT municipio_departamento_fk FOREIGN KEY (departamento_id)
        REFERENCES departamento (id);

ALTER TABLE sucursal
    ADD CONSTRAINT sucursal_municipio_fk FOREIGN KEY (municipio_id)
        REFERENCES municipio (id);

ALTER TABLE transaccion
    ADD CONSTRAINT transaccion_cuentas_fk FOREIGN KEY (cuentas_id)
        REFERENCES cuentas (id);

ALTER TABLE transaccion
    ADD CONSTRAINT transaccion_sucursal_fk FOREIGN KEY (sucursal_id)
        REFERENCES sucursal (id);
		
		

-----------------Procedimientos----------------------------

CREATE OR REPLACE PROCEDURE insertar_cliente(
    p_id VARCHAR(13),
    p_nombres VARCHAR(25),
    p_apellidos VARCHAR(25),
    p_razon VARCHAR(50),
    p_tipo_documento_id INTEGER,
    p_municipio_id INTEGER
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO cliente (id, nombres, apellidos, razon, tipo_documento_id, municipio_id)
    VALUES (p_id, p_nombres, p_apellidos, p_razon, p_tipo_documento_id, p_municipio_id);
END;
$$



CREATE OR REPLACE PROCEDURE crear_cuenta(
    p_cliente_id VARCHAR(13),
	p_moneda	CHAR(1)
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO cuentas (cliente_id, activa, moneda, saldo)
    VALUES (p_cliente_id, 'S', p_moneda, 0.00);
END;
$$



CREATE OR REPLACE PROCEDURE realizar_transaccion(
    p_descripcion VARCHAR(50),
    p_monto NUMERIC,
    p_cuentas_id INTEGER,
    p_sucursal_id INTEGER
)
LANGUAGE plpgsql
AS $$
DECLARE 
	v_saldo NUMERIC;
BEGIN
    
	
	IF LOWER(p_descripcion) = 'deposito' THEN
		INSERT INTO transaccion (descripcion, monto, cuentas_id, fechahora, sucursal_id)
    	VALUES (p_descripcion, p_monto, p_cuentas_id, NOW(), p_sucursal_id);
        
		UPDATE cuentas SET saldo = saldo + p_monto WHERE id = p_cuentas_id;
    
	ELSEIF LOWER(p_descripcion) = 'retiro' THEN
        
		SELECT saldo INTO v_saldo FROM cuentas where id = p_cuentas_id;
		IF v_saldo - p_monto < 0 THEN
			RAISE EXCEPTION 'Error saldo insuficiente.';
			RETURN;
		END IF;
		INSERT INTO transaccion (descripcion, monto, cuentas_id, fechahora, sucursal_id)
    	VALUES (p_descripcion, p_monto, p_cuentas_id, NOW(), p_sucursal_id);
		
		UPDATE cuentas SET saldo = saldo - p_monto WHERE id = p_cuentas_id;
    END IF;
END;
$$


